﻿
![picture](https://cdn2.steamgriddb.com/logo_thumb/84f3c1e3175163b2200bdf7edf808468.png) 

Jazz Jackrabbit 2 powered by Jazz² Resurrection

The binaries were compiled from the official Jazz² Resurrection repo
https://github.com/deathkiller/jazz2-native

Packages: 

[jazzjackrabbit2](https://aur.archlinux.org/packages/jazzjackrabbit2))

[jazz2-bin](https://aur.archlinux.org/packages/jazz2-bin))

 ### Author
  * Corey Bruce
